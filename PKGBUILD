# Maintainer: David Runge <dvzrv@archlinux.org>
# Contributor: muzhed <chustokes@126.com>

pkgname=din
pkgver=57
pkgrel=1
pkgdesc="A sound synthesizer and musical instrument."
arch=(x86_64)
url="https://dinisnoise.org/"
license=(GPL2)
groups=(pro-audio)
depends=(
  gcc-libs
  glibc
  hicolor-icon-theme
  libglvnd
  sdl
  tcl
)
makedepends=(
  boost
  glu
  rtaudio
  rtmidi
)
source=(
  https://archive.org/download/dinisnoise_source_code/$pkgname-$pkgver.tar.gz
  $pkgname-57-devendor_rtaudio_rtmidi.patch
)
sha512sums=('3d0c6773c5dd51699647004fecf30e586115b2b675e2eaf2be6d02078f271b1b07e6a3658211f5be36dac90a7f1cde62ed1cebe4fb82a9661aec892c49e94044'
            'daf86cbf0a6ea19f8afc60a0f0ed8f2e7864674414c08dc11c17562f849594747a60c2c3430d6b075804772d9136edc5271bd8df5ec1b3b4b42c1e9953fb2bd3')
b2sums=('85aec7f74a1e4b8f96a1deeb59a276f9fa30011344e8a22ebed766fcf6f0be14f78d622c27b954c8cf994afc068cf3b1d08b75657790217f1215f2d7587faed3'
        '751a4dfe87a0e68fdc67fa1a3f55ec178ca0e24014afd8e6c00145a6e2c8601228df0babc6796f57503826c0d97633069eced7a83363f40d90d261b6ee491164')

prepare() {
  # use system rtaudio/rtmidi, instead of vendored versions
  patch -Np1 -d $pkgname-$pkgver -i ../$pkgname-57-devendor_rtaudio_rtmidi.patch

  cd $pkgname-$pkgver
  rm -fv src/{RtAudio,RtMidi}.*
  autoreconf -fiv
}

build() {
  cd $pkgname-$pkgver
  export CXXFLAGS+=" -D__UNIX_JACK__ $(pkg-config --cflags rtaudio rtmidi)"
  export CFLAGS+=" -D__UNIX_JACK__ $(pkg-config --cflags rtaudio rtmidi)"
  export LIBS+=" $(pkg-config --libs rtaudio rtmidi)"
  ./configure --prefix='/usr'
  make
}

package() {
  depends+=(
    rtaudio librtaudio.so
    rtmidi librtmidi.so
  )

  make install DESTDIR="$pkgdir" -C $pkgname-$pkgver
  # docs
  install -vDm 644 $pkgname-$pkgver/{AUTHORS,BUGS,CHANGELOG,NEWS,README,TODO} -t "$pkgdir/usr/share/doc/$pkgname/"
}
